#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
from math import *
from sys import argv


def plot():
   r_l = 1.5 
   r_r = 5
   step = 0.001
   x0 = 0.5
   f = lambda r, x: r * x * (1 - x) 

   plt.figure()
   plt.grid(color='grey')
   plt.xlabel('r')
   plt.ylabel('x')

   R = np.arange(r_l, r_r, step)
   X = []
   for r in R:
      pts = simp_iter(x0, lambda x: f(r, x), 500)
      X.extend([(r, p[0]) for p in pts])
   r_pts, x_pts = zip(*X)

   print(len(X))

   plt.plot(r_pts, x_pts, 'r,')

   plt.show()

def simp_iter(x0, f, skip=0):
   EPS = 1e-6
   pts = []
   x = x0
   for iter in range(skip + 500):
      y = f(x)
      if iter > skip:
         pts.append((x, y))
         if abs(x - y) < EPS:
            break
      if abs(x) > 100 or abs(y) > 100:
         break
      x = y
   return pts


def main():
   plot()

if __name__ == '__main__':
   main() 
