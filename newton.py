#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
from math import *
from sys import argv


def colour(z):
   EPS = 1e-3
   r1 = 1
   r2 = -1./2 + sqrt(3) / 2 * 1j
   r3 = -1./2 - sqrt(3) / 2 * 1j
   if abs(z - r1) < EPS:
      return 1
   if abs(z - r2) < EPS:
      return 2
   if abs(z - r3) < EPS:
      return 3
   return 0

def plot():
   x0 = -1.5
   x1 = 1.5
   y0 = -1.5
   y1 = 1.5
   step = 0.02

   plt.figure()
   plt.xlabel('real')
   plt.ylabel('imagenary')

   X = np.arange(x0, x1, step)
   Y = np.arange(y0, y1, step)

   Z = np.empty((X.size, Y.size))
   Q = np.empty((X.size, Y.size))

   f = lambda z: z - (z ** 3 - 1) / (3 * z ** 2)

   for i in range(X.size):
      for j in range(Y.size):
         x = X[i]
         y = Y[j]
         z = x + y * 1j
         (res, _) = newton(z, f)
         Z[j, i] = colour(res)

   plt.contourf(X,Y, Z)

   start = 0.5 + 0.5 * 1j
   try:
      start = float(argv[1]) + float(argv[2]) * 1j
   except:
      if len(argv) >= 3:
         print 'Input re(z) = "%s", im(z) = "%s" is incorrect. Started with default point z = 0.5 + 0.5i' % (argv[1], argv[2])
      elif len(argv) == 2:
         print 'To less arguments, 2 required. Started with default point z = 0.5 + 0.5i'
      else:
         print 'Started with default point z = 0.5 + 0.5i'
   (ans, path) = newton(start, f, path = True)
   re = [z.real for z in path]
   im = [z.imag for z in path]
   
   plt.plot(re, im)
   plt.plot([ans.real], [ans.imag], 'g^')

   plt.show()


def newton(z0, f, path = False):
   MAX_ITER = 100
   EPS = 1e-6
   z = z0
   p = [z0]
   for itr in range(MAX_ITER):
      next_z = f(z)
      if path:
         p.append(next_z)
      if abs(z - next_z) < EPS:
         return (next_z, p)
      z = next_z

   return (z, p)

def main():
   plot()

if __name__ == '__main__':
   main()
