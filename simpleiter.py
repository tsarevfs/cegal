#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
from math import *
from sys import argv

def plot():
   x_l = -0.5
   x_r = 1.5
   step = 0.1
   r = 2.5
   x0 = 0.5
   f = lambda x: r * x * (1 - x)

   plt.figure()
   plt.grid(color='grey')
   plt.xlabel('x')
   plt.ylabel('y')

   pts = simp_iter(x0, f)
   X_iter = []
   Y_iter = []
   for p in pts:
      X_iter.append(p[0])
      Y_iter.append(p[1])

      X_iter.append(p[1])
      Y_iter.append(p[1])

   plt.plot(X_iter, Y_iter, 'r')

   x_l = min(x_l, min(X_iter))
   x_r = max(x_r, max(X_iter))

   X = np.arange(x_l, x_r, step)
   fx = np.array([f(x) + x for x in X])

   plt.plot(X, fx)
   plt.plot(X, X)
   plt.plot([x0], [f(x0) + x0], 'ro')

   plt.show()

def simp_iter(x0, f):
   EPS = 1e-6
   pts = []
   x = x0
   for iter in range(1000):
      y = f(x) + x
      pts.append((x, y))
      if abs(x - y) < EPS:
         break
      if abs(x) > 5 or abs(y) > 5:
         break
      x = y
   return pts





def main():
   plot()

if __name__ == '__main__':
   main() 
